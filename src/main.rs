use std::io::{self, Write};

use crossterm::event::{self, Event, KeyCode, KeyModifiers};
use figlet_rs::FIGfont;
use term_size;

fn clear_terminal() {
    print!("\x1B[2J\x1B[1;1H"); // ANSI escape sequence to clear screen
    io::stdout().flush().unwrap();
}
fn main() {
    crossterm::terminal::enable_raw_mode().unwrap();
    let mut number: i32 = 0;

    let standard_font = FIGfont::standard().unwrap();
    print_ascii_art(number.to_string(), &standard_font);
    loop {
        if event::poll(std::time::Duration::from_millis(500)).unwrap() {
            if let Event::Key(key_event) = event::read().unwrap() {
                match (key_event.code, key_event.modifiers) {
                    (KeyCode::Up, KeyModifiers::SHIFT)
                    | (KeyCode::Char('k'), KeyModifiers::SHIFT) => number += 10,
                    (KeyCode::Down, KeyModifiers::SHIFT)
                    | (KeyCode::Char('j'), KeyModifiers::SHIFT) => {
                        number -= if number > 9 { 10 } else { number }
                    }
                    (KeyCode::Up, _) | (KeyCode::Char('k'), _) => number += 1,
                    (KeyCode::Down, _) | (KeyCode::Char('j'), _) => {
                        number -= if number > 0 { 1 } else { 0 }
                    }
                    (KeyCode::Char('r'), _) => number = 0,
                    (KeyCode::Esc, _)
                    | (KeyCode::Char('q'), _)
                    | (KeyCode::Char('c'), KeyModifiers::CONTROL) => break,
                    _ => {}
                }
            }
            print_ascii_art(number.to_string(), &standard_font);
        }
    }
}
fn print_ascii_art(number: String, font: &FIGfont) -> () {
    crossterm::terminal::disable_raw_mode().unwrap();
    clear_terminal();
    let figure = font.convert(&number);
    let terminal_width = term_size::dimensions().map(|(w, _)| w).unwrap_or(120);
    let terminal_height = term_size::dimensions().map(|(_, h)| h).unwrap_or(120);
    if let Some(ref figure) = figure {
        println!("To exit, press 'q' or 'Esc' or 'Ctrl+c'.");
        print!("{}", "\n".repeat(terminal_height / 2 - 3));
        for line in figure.to_string().lines() {
            let padding = (terminal_width.saturating_sub(line.len())) / 2;
            println!("{:padding$}{}", "", line, padding = padding);
            // write!(io::stdout(), "{:padding$}{}", "", line, padding = padding).unwrap();
        }
        print!(
            "{}",
            "\n".repeat(terminal_height / 2 - figure.height as usize + 1)
        );
        println!("Press r to reset");
    } else {
        eprintln!("Failed to generate ASCII art.");
    }
    crossterm::terminal::enable_raw_mode().unwrap();
}
